package One_to_one.Bidirectional;

import javax.persistence.*;
import javax.persistence.OneToOne;


import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "master", catalog = "public", uniqueConstraints = {
        @UniqueConstraint(columnNames = "Name"),
        @UniqueConstraint(columnNames = "Surname") })
public class Master implements java.io.Serializable {

    private Integer Master_Id;
    private String Name;
    private String Surname;
    private Dog dog;

    public Master() {
    }

    public Master(String Name, String Surname) {
        this.Name = Name;
        this.Surname = Surname;
    }

    public Master(String Name, String Surname, Dog dog) {
        this.Name = Name;
        this.Surname = Surname;
        this.dog = dog;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Master_Id", unique = true, nullable = false)
    public Integer getMaster_Id() {
        return this.Master_Id;
    }

    public void setMaster_Id(Integer Master_Id) {
        this.Master_Id = Master_Id;
    }

    @Column(name = "Name", unique = true, nullable = false, length = 10)
    public String getName() {
        return this.Name;
    }

    public void setName(String stockCode) {
        this.Name = stockCode;
    }

    @Column(name = "Surname", unique = true, nullable = false, length = 20)
    public String getSurname() {
        return this.Surname;
    }

    public void setSurname(String stockName) {
        this.Surname = stockName;
    }

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "master", cascade = CascadeType.ALL)
    public Dog getDog() {
        return this.dog;
    }

    public void setDog(Dog dog) {
        this.dog = dog;
    }

}
