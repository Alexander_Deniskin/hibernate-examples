package One_to_one.Bidirectional;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import javax.persistence.OneToOne;

import java.util.Date;

@Entity
@Table(name = "dog", catalog = "public")
public class Dog implements java.io.Serializable {

    private Integer Master_Id;
    private String Nickname;
    private String Breed;
    private Master master;
    private Date listedDate;

    public Dog() {
    }

    public Dog(Master master,String Nickname, String Breed, Date listedDate) {
        this.master=master;
        this.Nickname = Nickname;
        this.Breed = Breed;
        this.listedDate = listedDate;
    }

    @GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "master"))
    @Id
    @GeneratedValue(generator = "generator")
    @Column(name = "Master_Id", unique = true, nullable = false)
    public Integer getMaster_Id() {
        return this.Master_Id;
    }

    public void setMaster_Id(Integer Master_Id) {
        this.Master_Id = Master_Id;
    }
    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public Master getMaster() {
        return this.master;
    }

    public void setMaster(Master master) {
        this.master = master;
    }

    @Column(name = "Nickname", nullable = false, length = 100)
    public String getNickname() {
        return this.Nickname;
    }

    public void setNickname(String Nickname) {
        this.Nickname = Nickname;
    }

    @Column(name = "Breed", nullable = false)
    public String getBreed() {
        return this.Breed;
    }

    public void setBreed(String Breed) {
        this.Breed = Breed;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "Date", nullable = false, length = 10)
    public Date getListedDate() {
        return this.listedDate;
    }

    public void setListedDate(Date listedDate) {
        this.listedDate = listedDate;
    }

}
