package One_to_one.Bidirectional;
import Source.HibernateUtil;
import org.hibernate.Session;

import java.util.Date;

public class One_to_One_bi{
    public static void app() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Master master = new Master();
        master.setName("Aleksandr");
        master.setSurname("Deniskin");
        Dog dog = new Dog();
        dog.setNickname("Sharik");
        dog.setBreed("Sheepdog");
        dog.setListedDate(new Date());
        master.setDog(dog);
        dog.setMaster(master);
        session.save(master);
        session.getTransaction().commit();
    }
}