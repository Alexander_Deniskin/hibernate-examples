package Source;

import Many_to_many.Bidirectional.Many_to_Many_bi;
import Many_to_many.Unidirectional.Many_to_Many_uni;
import One_to_many.Bidirectional.One_to_Many_bi;
import One_to_many.Unidirectional_Many_to_one.Many_to_One_uni;
import One_to_many.Unidirectional_One_to_many.One_to_Many_uni;
import One_to_one.Bidirectional.One_to_One_bi;
import One_to_one.Unidirectional.One_to_One_uni;

import java.util.Scanner;
public class App {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int key=0;
        while(key==0) {
            System.out.println("Make your choice:");
            System.out.println("1.One-to-one-bidirectional");
            System.out.println("2.One-to-one-unidirectional");
            System.out.println("3.One-to-Many-Bidirectional");
            System.out.println("4.One-to-Many-Unidirectional");
            System.out.println("5.Many-to-One-Unidirectional");
            System.out.println("6.Many-to-Many-Bidirectional");
            System.out.println("7.Many-to-Many-Unidirectional");
            key= in.nextInt();
            switch (key) {
                case 1:
                    One_to_One_bi.app();
                    break;
                case 2:
                    One_to_One_uni.app();
                    break;
                case 3:
                    One_to_Many_bi.app();
                    break;
                case 4:
                    One_to_Many_uni.app();
                    break;
                case 5:
                    Many_to_One_uni.app();
                    break;
                case 6:
                    Many_to_Many_bi.app();
                    break;
                case 7:
                    Many_to_Many_uni.app();
                    break;
                default:
                    System.out.println("Try again");
                    key=0;
                    break;
            }
        }
    }
}