package One_to_many.Bidirectional;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "dog", catalog = "public"/*,
        uniqueConstraints = @UniqueConstraint(columnNames = "DATE")*/)
public class Dog implements java.io.Serializable {

    private Integer Dog_Id;
    private Master Masters;
    private String Nickname;
    private String Breed;
/*    private Date listedDate;*/

    public Dog() {
    }
/*    public Dog(Master Masters,Date listedDate){
        this.Masters=Masters;
        this.listedDate=listedDate;
    }*/
    public Dog(Master Masters, String Nickname, String Breed/*, Date listedDate*/) {
        this.Masters=Masters;
        this.Nickname = Nickname;
        this.Breed = Breed;
/*        this.listedDate = listedDate;*/
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Dog_Id", unique = true, nullable = false)
    public Integer getDog_Id() {
        return this.Dog_Id;
    }

    public void setDog_Id(Integer Dog_Id) {
        this.Dog_Id = Dog_Id;
    }
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Master_Id", nullable = false)
    public Master getMaster() {
        return this.Masters;
    }

    public void setMaster(Master Masters) {
        this.Masters = Masters;
    }

    @Column(name = "Nickname", nullable = false, length = 100)
    public String getNickname() {
        return this.Nickname;
    }

    public void setNickname(String Nickname) {
        this.Nickname = Nickname;
    }

    @Column(name = "Breed", nullable = false)
    public String getBreed() {
        return this.Breed;
    }

    public void setBreed(String Breed) {
        this.Breed = Breed;
    }

/*    @Temporal(TemporalType.DATE)
    @Column(name = "Date", nullable = false, length = 10)
    public Date getListedDate() {
        return this.listedDate;
    }

    public void setListedDate(Date listedDate) {
        this.listedDate = listedDate;
    }*/

}
