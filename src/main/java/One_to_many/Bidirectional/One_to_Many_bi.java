package One_to_many.Bidirectional;

import Source.HibernateUtil;
import org.hibernate.Session;

import java.util.HashSet;
import java.util.Set;

public class One_to_Many_bi {
    public static void app() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Master master = new Master();
        master.setName("Aleksandr");
        master.setSurname("Deniskin");
        Dog dog1 = new Dog();
        Dog dog2 = new Dog();
        Dog dog3 = new Dog();
        dog1.setNickname("Sharik");
        dog1.setBreed("Volchara");
/*        dog1.setListedDate(new Date());*/
        dog2.setNickname("Tuzik");
        dog2.setBreed("Pes");
/*        dog2.setListedDate(new Date());*/
        dog3.setNickname("Bobik");
        dog3.setBreed("Sobaka");
/*        dog3.setListedDate(new Date());*/
        dog1.setMaster(master);
        dog2.setMaster(master);
        dog3.setMaster(master);
        session.save(dog1);
        session.save(dog2);
        session.save(dog3);
        Set<Dog> Dogs = new HashSet<Dog>(0);
        Dogs.add(dog1);
        Dogs.add(dog2);
        Dogs.add(dog3);
        master.setDogs(Dogs);
        session.save(master);
        session.getTransaction().commit();
    }
}