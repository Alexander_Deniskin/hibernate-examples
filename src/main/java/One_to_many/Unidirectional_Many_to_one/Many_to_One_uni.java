package One_to_many.Unidirectional_Many_to_one;


import Source.HibernateUtil;
import org.hibernate.Session;

public class Many_to_One_uni {
    public static void app() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Master master = new Master();
        master.setName("Aleksandr");
        master.setSurname("Deniskin");
        Dog dog1 = new Dog();
        Dog dog2 = new Dog();
        Dog dog3 = new Dog();
        dog1.setNickname("Sharik");
        dog1.setBreed("Volchara");
/*        dog1.setListedDate(new Date());*/
        dog2.setNickname("Tuzik");
        dog2.setBreed("Pes");
/*        dog2.setListedDate(new Date());*/
        dog3.setNickname("Bobik");
        dog3.setBreed("Sobaka");
/*        dog3.setListedDate(new Date());*/
        session.save(master);
        dog1.setMaster(master);
        dog2.setMaster(master);
        dog3.setMaster(master);
        session.save(dog1);
        session.save(dog2);
        session.save(dog3);
        session.save(master);
        session.getTransaction().commit();
    }
}