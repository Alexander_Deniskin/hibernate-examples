package One_to_many.Unidirectional_One_to_many;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "master", catalog = "public", uniqueConstraints = {
        @UniqueConstraint(columnNames = "Name"),
        @UniqueConstraint(columnNames = "Surname") })
public class Master implements java.io.Serializable {

    private Integer Master_Id;
    private String Name;
    private String Surname;
    private Set<Dog> dogs = new HashSet<Dog>(0);

    public Master() {
    }

    public Master(String Name, String Surname) {
        this.Name = Name;
        this.Surname = Surname;
    }

    public Master(String Name, String Surname, Set<Dog> dogs) {
        this.Name = Name;
        this.Surname = Surname;
        this.dogs = dogs;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Master_Id",unique = true, nullable = false)
    public Integer getMaster_Id() {
        return this.Master_Id;
    }

    public void setMaster_Id(Integer Master_Id) {
        this.Master_Id = Master_Id;
    }

    @Column(name = "Name", unique = true, nullable = false, length = 10)
    public String getName() {
        return this.Name;
    }

    public void setName(String stockCode) {
        this.Name = stockCode;
    }

    @Column(name = "Surname", unique = true, nullable = false, length = 20)
    public String getSurname() {
        return this.Surname;
    }

    public void setSurname(String stockName) {
        this.Surname = stockName;
    }

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "Master_Id")
    public Set<Dog> getdogs() {
        return this.dogs;
    }

    public void setdogs(Set<Dog> dogs) {
        this.dogs = dogs;
    }
}
