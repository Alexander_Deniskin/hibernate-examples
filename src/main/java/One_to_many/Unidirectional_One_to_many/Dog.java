package One_to_many.Unidirectional_One_to_many;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "dog", catalog = "public")
public class Dog implements java.io.Serializable {

    private Integer Dog_Id;
    private String Nickname;
    private String Breed;
/*    private Date listedDate;*/

    public Dog() {
    }
    public Dog(String Nickname, String Breed/*, Date listedDate*/) {
        this.Nickname = Nickname;
        this.Breed = Breed;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Dog_Id", unique = true, nullable = false)
    public Integer getDog_Id() {
        return this.Dog_Id;
    }

    public void setDog_Id(Integer Dog_Id) {
        this.Dog_Id = Dog_Id;
    }

    @Column(name = "Nickname", nullable = false, length = 100)
    public String getNickname() {
        return this.Nickname;
    }

    public void setNickname(String Nickname) {
        this.Nickname = Nickname;
    }

    @Column(name = "Breed", nullable = false)
    public String getBreed() {
        return this.Breed;
    }

    public void setBreed(String Breed) {
        this.Breed = Breed;
    }

/*    @Temporal(TemporalType.DATE)
    @Column(name = "Date", nullable = false, length = 10)
    public Date getListedDate() {
        return this.listedDate;
    }

    public void setListedDate(Date listedDate) {
        this.listedDate = listedDate;
    }*/

}
