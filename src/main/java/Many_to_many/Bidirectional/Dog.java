package Many_to_many.Bidirectional;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "dog", catalog = "public")
public class Dog implements java.io.Serializable {

    private Integer Dog_Id;
    private String Nickname;
    private String Breed;
    private Set<Master> master=new HashSet<Master>(0);

    public Dog() {
    }

    public Dog(String Nickname, String Breed) {
        this.Nickname = Nickname;
        this.Breed = Breed;
    }
    public Dog(Set<Master> master, String Nickname, String Breed) {
        this.master=master;
        this.Nickname = Nickname;
        this.Breed = Breed;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Dog_Id", unique = true, nullable = false)
    public Integer getDog_Id() {
        return this.Dog_Id;
    }

    public void setDog_Id(Integer Dog_Id) {
        this.Dog_Id = Dog_Id;
    }

    @Column(name = "Nickname", nullable = false, length = 100)
    public String getNickname() {
        return this.Nickname;
    }

    public void setNickname(String Nickname) {
        this.Nickname = Nickname;
    }

    @Column(name = "Breed", nullable = false)
    public String getBreed() {
        return this.Breed;
    }

    public void setBreed(String Breed) {
        this.Breed = Breed;
    }

    @ManyToMany(fetch = FetchType.LAZY,mappedBy = "dogs")
    public Set<Master> getmaster() {
        return this.master;
    }

    public void setmaster(Set<Master> master) {
        this.master = master;
    }

}
