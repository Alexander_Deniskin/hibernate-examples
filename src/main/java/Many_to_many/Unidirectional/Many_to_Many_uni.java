package Many_to_many.Unidirectional;

import Source.HibernateUtil;
import org.hibernate.Session;

import java.util.HashSet;
import java.util.Set;

public class Many_to_Many_uni {
    public static void app() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Master master = new Master();
        master.setName("Aleksandr");
        master.setSurname("Deniskin");
        Dog dog1 = new Dog("Sharik","Sheepdog");
        Dog dog2 = new Dog("Baron","Volchara");
        Set<Dog> dogs=new HashSet<Dog>();
        master.setDog(dogs);
        session.save(master);
        session.getTransaction().commit();
    }
}