package Many_to_many.Unidirectional;



import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "master", catalog = "public", uniqueConstraints = {
        @UniqueConstraint(columnNames = "Name"),
        @UniqueConstraint(columnNames = "Surname") })
public class Master implements java.io.Serializable {

    private Integer Master_Id;
    private String Name;
    private String Surname;
    private Set<Dog> Dogs=new HashSet<Dog>(0);

    public Master() {
    }

    public Master(String Name, String Surname) {
        this.Name = Name;
        this.Surname = Surname;
    }

        public Master(String Name, String Surname, Set<Dog> dogs) {
        this.Name = Name;
        this.Surname = Surname;
        this.Dogs = dogs;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Master_Id", unique = true, nullable = false)
    public Integer getMaster_Id() {
        return this.Master_Id;
    }

    public void setMaster_Id(Integer Master_Id) {
        this.Master_Id = Master_Id;
    }

    @Column(name = "Name", unique = true, nullable = false, length = 10)
    public String getName() {
        return this.Name;
    }

    public void setName(String stockCode) {
        this.Name = stockCode;
    }

    @Column(name = "Surname", unique = true, nullable = false, length = 20)
    public String getSurname() {
        return this.Surname;
    }

    public void setSurname(String stockName) {
        this.Surname = stockName;
    }

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "master_dog",catalog = "public",joinColumns = {
            @JoinColumn(name = "Master_Id",nullable = false,updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "Dog_Id",nullable = false,updatable = false)})
    public Set<Dog> getDog() {
        return this.Dogs;
    }

    public void setDog(Set<Dog> dogs) {
        this.Dogs = dogs;
    }

}
